package app.irvanyale.com.academy.ui.bookmark

import androidx.lifecycle.ViewModel
import app.irvanyale.com.academy.data.CourseEntity
import app.irvanyale.com.academy.utils.DataDummy

class BookmarkViewModel : ViewModel()  {
    fun getBookmarks(): List<CourseEntity> = DataDummy.generateDummyCourses()
}