package app.irvanyale.com.academy.ui.bookmark

import app.irvanyale.com.academy.data.CourseEntity

interface BookmarkFragmentCallback {
    fun onShareClick(course: CourseEntity)
}
