package app.irvanyale.com.academy.ui.academy

import androidx.lifecycle.ViewModel
import app.irvanyale.com.academy.data.CourseEntity
import app.irvanyale.com.academy.utils.DataDummy

class AcademyViewModel : ViewModel() {
    fun getCourses(): List<CourseEntity> = DataDummy.generateDummyCourses()
}